function varargout = MakeRavenInstallationInstructions(varargin)
% MAKERAVENINSTALLATIONINSTRUCTIONS MATLAB code for MakeRavenInstallationInstructions.fig
%      MAKERAVENINSTALLATIONINSTRUCTIONS, by itself, creates a new MAKERAVENINSTALLATIONINSTRUCTIONS or raises the existing
%      singleton*.
%
%      H = MAKERAVENINSTALLATIONINSTRUCTIONS returns the handle to a new MAKERAVENINSTALLATIONINSTRUCTIONS or the handle to
%      the existing singleton*.
%
%      MAKERAVENINSTALLATIONINSTRUCTIONS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAKERAVENINSTALLATIONINSTRUCTIONS.M with the given input arguments.
%
%      MAKERAVENINSTALLATIONINSTRUCTIONS('Property','Value',...) creates a new MAKERAVENINSTALLATIONINSTRUCTIONS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MakeRavenInstallationInstructions_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MakeRavenInstallationInstructions_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MakeRavenInstallationInstructions

% Last Modified by GUIDE v2.5 24-Jan-2020 08:02:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MakeRavenInstallationInstructions_OpeningFcn, ...
                   'gui_OutputFcn',  @MakeRavenInstallationInstructions_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MakeRavenInstallationInstructions is made visible.
function MakeRavenInstallationInstructions_OpeningFcn(hObject, eventdata, handles, varargin)

% Choose default command line output for MakeRavenInstallationInstructions
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = MakeRavenInstallationInstructions_OutputFcn(hObject, eventdata, handles) 


% Get default command line output from handles structure
varargout{1} = handles.output;



function serialNumber_Callback(hObject, eventdata, handles)

% Initializations
[~,p] = dos('ECHO %userprofile%');
p = p(1:end-1);
infilePath = fullfile(p,'Box\CCB Users\pitzrick_mike_msp2\Raven_support\Volusion\Installation_instructions\_BLANK');
outpath = fullfile(p,'Box\CCB Users\pitzrick_mike_msp2\Raven_support\Volusion\Installation_instructions');
ast = '*******************************************************************';
encodingIn = 'ISO-8859-1';

% Get serial number and customer name
input = hObject.String;
if(isempty(input))
    return;
end
inputC = strsplit(input,'\t');
serialNumber = strtrim(inputC{1});
customerName = strrep(inputC{2},' ','');

% Get license type
menuC = cellstr(get(handles.licenseType,'String'));
licenseType = menuC{get(handles.licenseType,'Value')};

% Find file name of installation instructions
switch licenseType
    case 'Raven Lite'
        infile = 'RavenLite.html';
    case 'Raven Pro - COMMERCIAL'
        infile = 'RavenPro_Commercial.html';
    case 'Raven Pro - free instructional evaluation'
        infile = 'RavenPro_free_instructional_evaluation.html';
    case 'Raven Pro - free trial'
        infile = 'RavenPro_free_trial.html';
    case 'Raven Pro - LAB'
        infile = 'RavenPro_Lab.html';
    case 'Raven Pro - SEMESTER'
        infile = 'RavenPro_Semester.html';
    case 'Raven Pro - STANDARD'
        infile = 'RavenPro_Standard.html';
    case 'Raven Pro - STANDARD (Spanish)'
        infile = 'RavenPro_Standard_Spanish.html';
    case 'Raven Pro - STANDARD (Portuguese)'
        infile = 'RavenPro_Standard_Portuguese.html';
    case 'Raven Pro - STUDENT'
        infile = 'RavenPro_Student.html';
    case 'Raven Pro - SUBSCRIPTION'
        infile = 'RavenPro_Subscription.html';
    case 'Raven Pro 2.0'
        infile = 'RavenPro2_Subscription.html';
    case 'Raven Pro 2.0 (French)'
        infile = 'RavenPro2_Subscription_French.html';
end

% Read installation instructions
infileFull = fullfile(infilePath,infile);
fid = fopen(infileFull,'rt','n',encodingIn);
assert(~isequal(fid,-1),sprintf('\n\n%s\nInstallation instructions could not be read:\n   %s\n%s\n',ast,infile,ast));
C = textscan(fid,'%s','Delimiter','\n');
C = C{1};
fclose(fid);

% Edit installation instructions
C = strrep(C,'$(body)',serialNumber);

% Write installation instructions
outfile = strrep(infile,'.html',['_',customerName,'.html']);
outpathFull = fullfile(outpath,outfile);
assert(~isfile(outpathFull),sprintf('\n\nWARNING: Output file already exists. File not written.\n\n\t%s\n',outfile))
fid = fopen(outpathFull,'wt','n',encodingIn);
assert(~isequal(fid,-1),'\n\nWARNING: Installation instructions could not be written:\n  %s\n',outpathFull)
fprintf(fid,'%s\n',C{:,:});
fclose(fid);

% Reinitialize input field
hObject.String = '';


% --- Executes during object creation, after setting all properties.
function serialNumber_CreateFcn(hObject, eventdata, handles)
% hObject    handle to serialNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in licenseType.
function licenseType_Callback(hObject, eventdata, handles)
% hObject    handle to licenseType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns licenseType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from licenseType


% --- Executes during object creation, after setting all properties.
function licenseType_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
